print("*Setup*")
wifiTrys     = 0    
NUMWIFITRYS  = 200  

function launch()
  print("Connected to WIFI!")
  print("IP Address: " .. wifi.sta.getip())
  print("Launching main.lc");
  dofile("main.lc");
end

function Trim(s)
  return (s:gsub("^%s*(.-)%s*$", "%1"))
end

function checkWIFI() 
  if ( wifiTrys > NUMWIFITRYS ) then
    print("Sorry. Not able to connect")
  else
    ipAddr = wifi.sta.getip()
    if ( ( ipAddr ~= nil ) and  ( ipAddr ~= "0.0.0.0" ) )then
      tmr.alarm( 1 , 500 , 0 , launch )
    else
      -- Reset alarm again
      tmr.alarm( 0 , 5000 , 0 , checkWIFI)
      print("Checking WIFI..." .. wifiTrys)
      wifiTrys = wifiTrys + 1
    end 
  end 
end

print("-- Starting up! ")

print("-- Reading config.txt")
file.open("config.txt", "r")
ssid=Trim(file.readline())
pw=Trim(file.readline())
print("WiFi SSID:"..ssid);
print("WiFi PW:  "..pw);
file.close()

ipAddr = wifi.sta.getip()
if ( ( ipAddr == nil ) or  ( ipAddr == "0.0.0.0" ) ) then
  print("Configuring WIFI....")
  wifi.setmode( wifi.STATION )
  wifi.sta.config( ssid , pw)
  print("Waiting for connection")
  tmr.alarm( 0 , 5000 , 0 , checkWIFI )
else
 launch()
end