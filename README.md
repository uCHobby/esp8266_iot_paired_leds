uCHobby Project ESP8266 IoT Paired LEDs
================
This project/experiment uses two ESP8266-1 modules to make a set of paired LEDs. Each module has a button and an LED. They connect to an MQTT broker to share the LED status. Press the button on one module and it's LED comes on, it sends that button to the other module and it's LED comes on.  Press again and both go off.

A post at uChobby shows more about this project. http://www.uchobby.com/index.php/2015/06/01/esp8266-iot-paired-leds/

Think of this like a signal system.  You want your wife to call you, instead of interrupting her, you signal. One of these modules would be placed in the kitchen, the other at the office on your desk. Yes, sexist I know, but leave me alone. For this example, assume we are talking about June and Ward Cleaver.

Ward wants June to call so he presses the button, his light goes on and seconds later June sees it as she is making cookies for the kid.  She presses the button to let Ward know she got the message.  Ward's light goes out seconds later, he knows his good wife will call when she has a moment.

I imagine there are a lot of uses for a signal pair like this...

Each module includes
    There are two modules, called them "N1" and "N2" in the code and MQTT messages
        ESP-01 Module - I give pin names below from the ESP-01 breakout board I used.
        Push Button connected to GP0.
            Use a 4.7K resistor pull up so that pin is normally high, pressing the button grounds the pin. 
            You might want a small cap across the switch to help de-bounce, I used a .001uF
        LED connected to GP2 
            I used a 330 Ohm resistor to limit LED current, with the LED connected to ground. You may need to adjust the resistor depending on your LED.

Other Connections
    Connect VCC and CHPD to 3.3V
        The ESP-01 modules need 3.3V DC and lots of it. Watch out for sags in the supply when the modules are active.

This project uses NodeMCU, which makes the ESP module in to a Lua interpreter. Lua is a tiny language used to drive the module as a standalone device. The program is stored in a small serial memory on the module and space is very tight, both in the memory chip and in RAM where the program is loaded. The code is not commented as they would eat up space, and is very terse to save RAM. These limits are a major pain for someone who likes good programming practices.

There are four files used on the ESP module, each is described below
    init.lua
        Short and sweet lua file that fires off the compiled setup.lc file described below. Init.lua is automatically run at power on and must be a 
        straight lua file, not compiled. To conserve space, this file is just a short startup to load setup.lc
    config.txt
        Stores the various settings needed by each module. This file changes based on the module name while the other files are common to both.
        One line for each setting as follows. No extra spaces around the items, just the necessary text and nothing extra.  Note that the package includes an
        example config file. 
            wifi SSID
            wifi password
            This node's name
            Paired node's name
            MQTT IPAddress
    setup.lua - compiled to setup.lc
        This module gets things started. I did a good bit of research to find basic steps here and ended up with this version which seems to work well.  
        Study the code a bit and you can see that it's just concerned with getting a wi-fi connection.  Once it has wi-fi, it calls main.lc
    main.lua - compiled to main.lc
        This is the main program and is loaded by setup.lc when wi-fi is up and running.  A connection to the MQTT broker is established and a status message is           published every second. A message is subscribed as well, this is how the module finds out that it's pair's button was pressed.

Utility Software
    luatool  https://github.com/4refr0nt/luatool
        a python utility to load the module via a serial port.
    lua Loader  http://benlo.com/esp8266/
        another file tool, but I don’t understand how to make this work correctly.  I do use it to "format" memory and that is about all.
    Adobie Brackets  http://brackets.io/
        Awesome project editor, especially good for web projects.
    Atlassian Source Tree  https://www.sourcetreeapp.com/
        Use this to manage your Git source code system
    BitBucket  https://bitbucket.org/uCHobby
        Where I will be keeping my source. I'm a bit new to this system.

Extras
    Power and Primary Serial
        BreadboardBuddy Pro https://www.tindie.com/products/AtomSoft/breadboardbuddy-pro/
        I use this to get power from USB and as a serial connection. I really like this breadboard tool.
    
    ESP-01 Breadboard Adapter https://www.tindie.com/products/rajbex/esp8266-breadboard-adapter/
        This is how I connect the ESP module to my breadboard.    
    
    Second Serial, FTDI, Optional https://www.sparkfun.com/products/9717
        I use an FTDI serial cable connected to the TX output of a module I'm working on. This is extra so I can keep it connected all the time. 
        The primary serial port is on a second USB serial interface which is TX/RX and used by the lua utilities. You don't have to do this.
    
NOTES:
    To List the files on the module
    C:\Sync\Development\Projects\ESP8266\esp8266_iot_paired_leds>python luatool.py --port COM2 -l
   
    To load the main.lua file and compile it on the module. You should "format" the module, power it down and back up before doing this.
    C:\Sync\Development\Projects\ESP8266\esp8266_iot_paired_leds>python luatool.py --port COM2 --src main.lua --dest main.lua --baud 9600 --verbose -c

    To Load the init.lua file on the module, note this file cannot be compiled so it's very short
    C:\Sync\Development\Projects\ESP8266\esp8266_iot_paired_leds>python luatool.py --port COM2 --src init.lua --dest init.lua --baud 9600 --verbose
    
    To load the setup.lua file which is compiled to make it smaller.
    C:\Sync\Development\Projects\ESP8266\esp8266_iot_paired_leds>python luatool.py --port COM2 --src setup.lua --dest setup.lua --baud 9600 --verbose -c
    
    To put the config file on the module. Note that I rename as I do this.  I have an N1 and N2 config file. Load on the module as needed.
    C:\Sync\Development\Projects\ESP8266\esp8266_iot_paired_leds>python luatool.py --port COM2 --src N1.config.txt --dest config.txt --baud 9600 --verbose
