print("*Main: IOT1 V1")
LEDState=false
stateCount=0
ledChange=false
mqttBlock=false
function Trim(s)
  return (s:gsub("^%s*(.-)%s*$", "%1"))
end
function MQTT_OnConnect()
    print("MQTT!")
    main()
end
file.open("config.txt", "r")
file.readline()
file.readline()
thisName=Trim(file.readline())
pairName=Trim(file.readline())
mqttIP=Trim(file.readline())
file.close()
m = mqtt.Client( "IOT1/" .. thisName, 120, "", "")
m:connect( mqttIP , 1883, 0, MQTT_OnConnect)
function MQTT_OnMessage(conn, topic, payload)
    mqttBlock=true;
    local topic=Trim(topic)
    local payload=Trim(payload)
    print("<MQTT " , topic , payload)
    
    if (payload == "1") then
        LEDSet(true)
    end
    if (payload == "0") then
        LEDSet(false)
    end
    mqttBlock=false
end
function LEDSet(state)
    if (state == true) then
        gpio.write(4,gpio.HIGH)
    else
        gpio.write(4,gpio.LOW)        
    end
    ledState=state
end
function OnButton(level)
    if(ledState) then
        LEDSet(false)
    else
        LEDSet(true)
    end
    ledChange=true
end
function OnTimer()
    if (stateCount==0) then
        if(mqttBlock~=ture) then
            mqttBlock=true;
            m:publish("IOT1/"..thisName.."/POR","1",0,0, function(conn) mqttBlock=false end)
            stateCount=stateCount+1
            return
        end
    end
    if (stateCount==1) then        
        if(mqttBlock~=ture) then
            mqttBlock=true
            m:subscribe("IOT1/"..thisName.."/LED", 0, function(conn) mqttBlock=false end)
            stateCount=stateCount+1
            return
        end
    end
    if (stateCount==2) then
        if(ledChange==true) then
            if(mqttBlock~=ture) then
                mqttBlock=true;
                m:publish("IOT1/"..pairName.."/LED", ledState and 1 or 0,0,0, function(conn) mqttBlock=false end)
                ledChange=false
            end
        else
            if(mqttBlock~=ture) then
                mqttBlock=true;
                m:publish("IOT1/"..thisName.."/STA", ledState and 1 or 0,0,0, function(conn) mqttBlock=false end)
            end
        end
    end
end        
    
function main()     
     gpio.mode(4,gpio.OUTPUT)
     gpio.trig(3, "up", OnButton)
     tmr.alarm(0, 1000, 1, OnTimer )
     m:on("message", MQTT_OnMessage)
end